This project is use Node.js

## BOT
Bot name [@oursky_larry_bot](https://web.telegram.org/#/im?p=@oursky_larry_bot)

## Install

Install [Node.js](https://nodejs.org/en/download/current/)

Windows : [https://nodejs.org/dist/v6.2.0/node-v6.2.0-x86.msi](https://nodejs.org/dist/v6.2.0/node-v6.2.0-x86.msi)

Linux : [https://nodejs.org/en/download/package-manager/](https://nodejs.org/en/download/package-manager/)

Install npm
```
sudo apt-get install npm
```

## Run

Change directory to project
```
node index.js
```

## Error

If has this error
```
xmljs.node: wrong ELF class: ELFCLASS32
```

use this command
```
npm install bindings
```