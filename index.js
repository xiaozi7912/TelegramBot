var express = require('express');
var telegramBot = require('node-telegram-bot');
var scheduler = require('node-schedule');
var querystring = require('querystring');
var http = require('http');
var request = require('request');
var libxmljs = require("libxmljs");
var fs = require("fs");

var tBot;
var myBot = {
	"file_subscriber_path": "subscriber.json",
	"name": "@oursky_larry_bot",
	"action": {
		"topics": {
			"data": ["current", "warning"],
			"method": actionTopics
		},
		"tellme": {
			"current": {
				"eng": "http://rss.weather.gov.hk/rss/CurrentWeather.xml",
				"cht": "http://rss.weather.gov.hk/rss/CurrentWeather_uc.xml",
				"chs": "http://gbrss.weather.gov.hk/rss/CurrentWeather_uc.xml",
				"method": tellMeCurrent
			},
			"warning": {
				"eng": "http://rss.weather.gov.hk/rss/WeatherWarningBulletin.xml",
				"cht": "http://rss.weather.gov.hk/rss/WeatherWarningBulletin_uc.xml",
				"chs": "http://gbrss.weather.gov.hk/rss/WeatherWarningBulletin_uc.xml",
				"method": tellMeWarning
			}
		},
		"subscribe": {
			"subscriber": [],
			"warning": function(message) {
				var exists = false;
				for (var i = 0; i < myBot.action.subscribe.subscriber.length; i++) {
					if (message.chat.id == myBot.action.subscribe.subscriber[i]) {
						exists = true;
						break;
					}
				}
				if (!exists) {
					myBot.action.subscribe.subscriber.push(message.chat.id);
				}
				console.log(myBot.action.subscribe.subscriber);

				fs.writeFile(myBot.file_subscriber_path, JSON.stringify(myBot.action.subscribe.subscriber), function(err) {
					if (err) {
						return console.error(err);
					}

					tBot.sendMessage({
						"chat_id": message.chat.id,
						"text": "Ok"
					}, function(result) {
						console.log(result);
					});
				});
			}
		},
		"unsubscribe": {
			"warning": function(message) {
				for (var i = 0; i < myBot.action.subscribe.subscriber.length; i++) {
					if (message.chat.id == myBot.action.subscribe.subscriber[i]) {
						myBot.action.subscribe.subscriber.splice(i, 1);
						break;
					}
				}
				console.log(myBot.action.subscribe.subscriber);

				fs.writeFile(myBot.file_subscriber_path, JSON.stringify(myBot.action.subscribe.subscriber), function(err) {
					if (err) {
						return console.error(err);
					}

					tBot.sendMessage({
						"chat_id": message.chat.id,
						"text": "Ok"
					}, function(result) {
						console.log(result);
					});
				});
			}
		},
		"繁體中文": {
			"method": function(message) {
				myBot.language = "cht"
				tBot.sendMessage({
					"chat_id": message.chat.id,
					"text": "轉換為繁體中文"
				}, function(result) {
					console.log(result);
				});
			}
		},
		"简体中文": {
			"method": function(message) {
				myBot.language = "chs"
				tBot.sendMessage({
					"chat_id": message.chat.id,
					"text": "转换为简体中文"
				}, function(result) {
					console.log(result);
				});
			}
		},
		"English": {
			"method": function(message) {
				myBot.language = "eng"
				tBot.sendMessage({
					"chat_id": message.chat.id,
					"text": "Change to English."
				}, function(result) {
					console.log(result);
				});
			}
		}
	},
	"language": "eng"
};

fs.readFile(myBot.file_subscriber_path, function(err, data) {
	if (err) {
		return console.error(err);
	}
	myBot.action.subscribe.subscriber = JSON.parse(data.toString());
	console.log(myBot);
	console.log("Asynchronous read: " + data.toString());
	for (var i = 0; i < myBot.action.subscribe.subscriber.length; i++) {
		console.log(myBot.action.subscribe.subscriber[i]);
	}

	tBot = new telegramBot({
		token: '219774439:AAHkFpz-FXMYURiUPEyXKkFERT3pE0BZm8U'
	})
		.on('message', function(message) {
			console.log(message);
			if (message.entities !== undefined) {
				var offset = message.entities[0].offset;
				var length = message.entities[0].length;

				console.log("myBot.name.length : " + myBot.name.length);
				console.log("message.entities[0].offset : " + message.entities[0].offset);
				console.log("message.entities[0].length : " + message.entities[0].length);
				console.log(message.text.split(" "));
				var action = message.text.split(" ")[1];

				if (offset == 0 && length == myBot.name.length) {
					if (action === "tellme") {
						var type = message.text.split(" ")[2];
						myBot.action[action][type].method(message);
					} else if (action === "subscribe") {
						var type = message.text.split(" ")[2];
						if (myBot.action[action][type] !== undefined) {
							myBot.action[action][type](message);
						}
					} else if (action === "unsubscribe") {
						var type = message.text.split(" ")[2];
						if (myBot.action[action][type] !== undefined) {
							myBot.action[action][type](message);
						}
					} else {
						myBot.action[action].method(message);
					}
				}
			} else {
				tBot.sendMessage({
					"chat_id": message.chat.id,
					"text": "@oursky_larry_bot topics"
				}, function(result) {
					console.log(result);
				});
			}
		})
		.start();

	pullingRss();
});

function actionTopics(message) {
	console.log("actionTopics");
	console.log(message);
	var response = "";
	for (var i = 0; i < myBot.action.topics.data.length; i++) {
		if (i != (myBot.action.topics.data.length - 1)) {
			response += myBot.action.topics.data[i] + ", ";
		} else {
			response += myBot.action.topics.data[i]
		}
	}
	tBot.sendMessage({
		"chat_id": message.chat.id,
		"text": response
	});
}

function tellMeCurrent(message) {
	console.log("tellMeCurrent");
	request.get(myBot.action.tellme.current[myBot.language], function(error, response, body) {
		if (!error && response.statusCode == 200) {
			var xmlDoc = libxmljs.parseXmlString(body);
			var gchild = xmlDoc.find('//description');

			if (gchild.length != 0) {
				var regex = /<p>([\w\d\s\.:<="'>/]*)<\/p>/gi;
				var res = regex.exec(gchild[1].text());
				for (var i = 0; i < res.length; i++) {
					console.log(res[i]);
				}
				var result = res[1].replace(/<br\/>/g, "").replace(/\r\n/g, "").replace(/[\s]{2,}/g, " ").replace(/<font color="red"><SPAN id='warning_message' >/, "").replace(/<\/SPAN> <\/font><p>/, "");

				console.log("result : " + result);

				tBot.sendMessage({
					"chat_id": message.chat.id,
					"text": result
				});
			} else {
				tBot.sendMessage({
					"chat_id": message.chat.id,
					"text": "Error."
				});
			}
		}
	});
}

function tellMeWarning(message) {
	console.log("tellMeWarning");
	request.get(myBot.action.tellme.warning[myBot.language], function(error, response, body) {
		if (!error && response.statusCode == 200) {
			var xmlDoc = libxmljs.parseXmlString(body);
			var gchild = xmlDoc.find('//description');

			if (gchild.length != 0) {
				var result = gchild[1].text().replace(/<br\/>/g, "");

				console.log("result : " + result);

				tBot.sendMessage({
					"chat_id": message.chat.id,
					"text": result
				});
			} else {
				tBot.sendMessage({
					"chat_id": message.chat.id,
					"text": "Error."
				});
			}
		}
	});
}

function pullingRss() {
	console.log("pullingRss");
	var lastPushDate = "";
	scheduler.scheduleJob('*/1 * * * *', function() {
		console.log("scheduleJob");
		console.log(myBot.action.tellme.warning[myBot.language]);
		request.get(myBot.action.tellme.warning[myBot.language], function(error, response, body) {
			if (!error && response.statusCode == 200) {
				// console.log(body);
				var xmlDoc = libxmljs.parseXmlString(body);
				var gchild = xmlDoc.find('//item//title');
				var pushDateNode = xmlDoc.find('//pubDate');

				if (pushDateNode.length != 0) {
					pushDate = pushDateNode[0].text();
					console.log("pushDate : " + pushDate);
					console.log("lastPushDate : " + lastPushDate);
					if (lastPushDate !== pushDate) {
						lastPushDate = pushDate;

						if (gchild.length != 0) {
							var result = gchild[0].text().replace(/<br\/>/g, "");
							console.log("result : " + result);

							for (var i = 0; i < myBot.action.subscribe.subscriber.length; i++) {
								var chat_id = myBot.action.subscribe.subscriber[i];
								tBot.sendMessage({
									"chat_id": chat_id,
									"text": result
								});
							}
						} else {
							for (var i = 0; i < myBot.action.subscribe.subscriber.length; i++) {
								var chat_id = myBot.action.subscribe.subscriber[i];
								tBot.sendMessage({
									"chat_id": chat_id,
									"text": "Error."
								});
							}
						}
					}
				}
			}
		});
	});
}